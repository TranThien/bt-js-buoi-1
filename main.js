// bài tập 1 : tỉnh tiền lương nhân viên
// * Đầu vào : thông tin lương của 1 ngày và thông tin số ngày làm người dùng nhập vào
var luongNgay = 100.0;

var soNgayLam = 16; // giả sử người dùng nhập vào là 16 ngày

// Xử lý thuật toán
var tongLuong = luongNgay * soNgayLam;

// Đầu ra
console.log("tongLuong:", tongLuong);

// Bài tập 2 :

// Đầu vào : thông tin của 5 số thực dc người dùng nhập vào là:

var soThuNhat = 5;
var soThuHai = 10;
var soThuBa = 15;
var soThuTu = 20;
var soThuNam = 25;

// Xử lý thuật toán
var average = (soThuNhat + soThuHai + soThuBa + soThuTu + soThuNam) / 5;

// Đầu ra :

console.log("average:", average);

// Bài tập 3 :
// Đầu vào : Nhập vào giá USD hiện nay và số tiền người dùng cần quy đổi

var cost = 23.5;
var soTienQuyDoi = 2;

//Xử lý thuật toán

var total = cost * soTienQuyDoi;
total = total.toFixed(3);

// Đầu ra
console.log("total:", total);

// Bài tập 4 :
// Đầu vào : nhập chiều dài và chiều rộng của hình chữ nhật , giả sử : chiều rộng : 15 và chiều dài : 25

var chieuDai = 25;
var chieuRong = 15;

// Xử lý thuật toán

var dienTich = chieuDai * chieuRong;
var chuVi = (chieuDai + chieuRong) * 2;

// Đầu ra
console.log("dienTich:", dienTich);
console.log("chuVi:", chuVi);

// Bài tập 5
// Đầu vào : Nhập 1 ký số có 2 chữ số

var n = 44;

//Xử lý thuật toán
var ten = Math.floor((n / 10) % 10);
var unit = Math.floor(n % 10);
var sum = ten + unit;

// Đầu ra :

console.log("sum:", sum);